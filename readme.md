# Machine learning tools for remote sensing data

---

## Installation

Depends on Python 3, numpy, pandas, scikit-learn, GDAL, lxml

### PYTHON environment

Try the installation of Miniconda (or the full Anaconda) package [link: <https://docs.continuum.io/anaconda/>]. Use the Python 3.5 version.

### GDAL installation

#### For Windows

Check <https://pypi.python.org/pypi/GDAL/> for official solutions.

An alternative solution is to download the customized package from [Christoph Gohlke's Page](http://www.lfd.uci.edu/~gohlke/pythonlibs/#gdal). Assuming that the PC has 64-bit Windows and Python 3.5 Miniconda environment installed, we need to download the corresponding package, and run `pip install`:

    pip install GDAL-2.0.3-cp35-cp35m-win_amd64.whl

#### For Mac

Recommend to use the `conda-forge` package. After the installation of Miniconda (Anaconda), run

    conda config --add channels conda-forge
    conda install gdal

### Install package: learn2map

    pip install learn2map


---

## Command-Line estimator

### rf_estimator -- Random Forest Estimator

#### Usage:

~~~~~~
rf_estimator -i "input" -o "outname" [-n "n_trees_step" -s "min_smpl_step" -f "max_ftr_step"]
~~~~~~

* `"input"` - location of input filename (e.g. `'./input_file_list.txt'`). It should be a txt file includes the individual input filename at each line (all files should be in `geotiff` format registered to have the same dimension and spatial resolution):

     1. 1st line specifies the filename of the mask file (could be a file denoting 0 as invalid, and 1 as valid pixels).

     2. 2nd line specifies the filename of the interested variable (e.g. the file containing AGB measurements for some pixels).

     3. All the rest lines can be filenames of environmental layers used as input data for spatial mapping.

* `"outname"` - location of output filename with no file extension, as multiple files will be generated using it as basename (e.g. `'./output/drc_agb'`).

* `n`, `s`, `f` - parameters used in algorithm optimization. It accepts integer values. Small numbers will increase the running time of the algorithm.

* **Final output**: geotiff file named as `"outname"_env_est_raster.tif`. It contains the prediction map of your interested variable -- the 2nd layer specified in `"input"`.

---

### raster_tools.py

Out-of-core tools for raster data analysis.

#### Requirements:

PYTHON 3, NUMPY, PANDAS, GDAL (with PYTHON support), LXML (Optional)

#### Functions:

1. raster_clip - For every input raster file, get the same spatial resolution,
projection, and extent as the reference raster.

2. build_stack_vrt - Build stacked virtual raster for all registered rasters.

3. raster_to_h5 - Out-of-core reformat of rasters to pandas dataframe using the
virtual raster as input. Dataframe is stored in hdf5 format.

4. ogrvrt_to_grid - Convert scattered points to raster. Point data set is stored
in csv format, and output is in geotiff format.

5. h5_to_csv - Reformat h5 to csv file in chunks.

---

### data_learning.py

Machine learning model setup.

#### Requirements:

PYTHON 3, NUMPY, PANDAS, sklearn, xgboost, seaborn (Optional),

#### Class:

GridLearn - Build the machine learning object.

* Properties
  - in_file: input h5 file that contains the training data
  - y_column: column index for response variable y
  - mask_column: column index for mask
  - best_params: param set that can be used in further learning
  - mdl: defined model pipeline

* Methods
  - setup_rfbc_model: Bias-corrected random forest model ([reference](http://cbmjournal.springeropen.com/articles/10.1186/s13021-016-0062-9))
  - setup_xgb_model: Set up xgboost model ([xgboost page](https://github.com/dmlc/xgboost))
  - tune_param_set: Find the best parameter set that is used in learning.
  - sklearn_test: sklearn and prediction for big data

---

### Other Functions

learning_xgb_big - out-of-core xgboost learning and prediction for big data.
