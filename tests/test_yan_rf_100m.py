import os
import sys

import raster_tools as rt


# import data_learning as dl


def set_input_data():
    """

    :rtype: dictionary of input files
    """
    os.chdir(sys.path[0])
    print(sys.path[0])
    data_path = '/Users/xuliang/Documents/yy/amazon_alos'
    data_files = {'data_path': data_path,
                  'out_path': os.path.join(data_path, 'out100m'),
                  'file_mask': os.path.join(data_path, 'vcf_0001deg.tif'),
                  'file_x1': [os.path.join(data_path, r) for r in
                              "yan002_hhmean.tif yan002_hvmean.tif"
                                  .split()],
                  'file_x2': [os.path.join(data_path, r) for r in
                              "srtm_amazon_v3.tif"
                                  .split()]
                  }
    return data_files


def set_input_data2():
    """

    :rtype: dictionary of input files
    """
    os.chdir(sys.path[0])
    print(sys.path[0])
    data_path = '/Users/xuliang/Documents/yy'
    data_files = {'data_path': os.path.join(data_path, 'hensen_loss'),
                  'out_path': os.path.join(data_path, 'Amazon_glas_jan2016'),
                  'file_mask': os.path.join(data_path, 'Amazon_glas_jan2016/076_ikmeans_mean9_geo.tif'),
                  'file_x1': [os.path.join(data_path, 'hensen_loss', r) for r in
                              "loss_year2001.tif loss_year2002.tif loss_year2003.tif loss_year2004.tif "
                              "loss_year2005.tif loss_year2006.tif loss_year2007.tif loss_year2008.tif "
                              "loss_year2009.tif loss_year2010.tif loss_year2011.tif loss_year2012.tif "
                              "loss_year2013.tif loss_year2014.tif"
                                  .split()],
                  }
    return data_files


input_files = set_input_data()
input_mask = input_files['file_mask']
# for i, file_name in enumerate(input_files['file_x1']):
#     print(i, file_name)
#     output_x = '{}_100m.tif'.format(os.path.splitext(file_name)[0])
#     rt.raster_clip(input_mask, file_name, output_x, 'bilinear')
output_x = '{}_100m.tif'.format(os.path.splitext(input_files['file_x2'][0])[0])
rt.raster_clip(input_mask, input_files['file_x2'][0], output_x, 'average')

# input_files = set_input_data2()
# input_mask = input_files['file_mask']
# for i, file_name in enumerate(input_files['file_x1']):
#     print(i, file_name)
#     output_x = '{}_5km.tif'.format(os.path.splitext(file_name)[0])
#     rt.raster_clip(input_mask, file_name, output_x, 'average')
