import os
import sys
import pandas as pd
import subprocess

import raster_tools as rt
# import data_learning as dl


def set_input_data_2():
    """

    :rtype: dictionary of input files
    """
    os.chdir(sys.path[0])
    print(sys.path[0])
    data_path = os.path.join('..', 'data', 'tmp')
    data_files = {'data_path': data_path,
                  'out_path': os.path.join(data_path, 'output'),
                  'file_mask': os.path.join(data_path, 'mask.tif'),
                  'file_y': os.path.join(data_path, 'test00.tif'),
                  'file_x': [os.path.join(data_path, r) for r in
                             "test00.tif test01.tif test02.tif test03.tif test04.tif test05.tif"
                                 .split()]
                  }
    return data_files

## define reference mask and input layers
input_files = set_input_data_2()
input_mask = input_files['file_mask']

## register input files to mask
in_file_list = os.path.join(input_files['out_path'], 'input_file_test.txt')
# rt.raster_clip_batch1(input_mask, in_file_list, input_files['file_x'])

## make vrt file for all input files
out_vrt = os.path.join(input_files['out_path'], 'test_set.vrt')
# field_names = rt.build_stack_vrt(in_file_list, out_vrt)

## save valid pixels to hdf5
out_h5 = '{}.h5'.format(os.path.splitext(out_vrt)[0])
# rt.raster_to_h5(out_vrt, out_h5, field_names, 'test05_reg_b1', 0, 100)

## convert hdf5 to csv
with pd.HDFStore(out_h5, mode='r') as store:
    df_columns = store.get_storer('df0').data_columns
    print(df_columns)
out_csv = '{}.csv'.format(os.path.splitext(out_h5)[0])
# rt.h5_to_csv(out_h5, out_csv)

## rasterize csv to geotiff using mask as reference
out_y = '{}_LidarMCH'.format(os.path.splitext(out_csv)[0])
rt.ogrvrt_to_grid(input_mask, out_csv, 'x', 'y', 'test05_reg_b1', out_y, a_interp='average:min_points=1')
# rt.ogrvrt_to_grid(input_mask, out_csv, 'x', 'y', 'test05_reg_b1', out_y, a_interp='nearest')

