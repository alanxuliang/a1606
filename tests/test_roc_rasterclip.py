import os
import sys
import subprocess

import raster_tools as rt


def set_input_data():
    """

    :rtype: dictionary of input files
    """
    os.chdir(sys.path[0])
    print(sys.path[0])
    # data_path = '/Users/xuliang/Documents/Codes/pcprojects/a1606_raster2stats/data/tmp/gee_roc'
    data_path = '/Volumes/MyBookThunderboltDuo/Archived_projects/p06_drc_lidar/002_DRC_country/googleee/le7toa_nbar_drc_2000-03'
    data_files = {'data_path': data_path,
                  'out_path': os.path.join(data_path, 'output'),
                  # 'file_key1': 'lc8_nbar_roc',
                  'file_key1': 'lc8_nbar_drc_2000',
                  }
    return data_files


input0 = set_input_data()
file1_name = input0['file_key1']
for i in range(4):
    file1 = []
    for r in os.scandir(input0['data_path']):
        if file1_name in r.name and r.name.endswith('.tif'):
            file_name0 = os.path.join(input0['out_path'], '{}_b{}.tif'.format(os.path.splitext(r.name)[0], i + 1))
            gdal_expression = (
                'gdal_translate -ot Float32 -a_nodata nan -b {} -of GTiff -co COMPRESS=LZW -co PREDICTOR=2 -co BIGTIFF=YES "{}" "{}"').format(
                i+1, r.path, file_name0)
            print(gdal_expression)
            subprocess.check_output(gdal_expression, shell=True)
            file1.append(file_name0)
    file1_string = ' '.join('"{}"'.format(w) for w in file1)
    file1_mosaic = os.path.join(input0['out_path'], '{}_b{}.tif'.format(file1_name, i+1))
    gdal_expression = (
        'gdal_merge.py -co COMPRESS=LZW -co PREDICTOR=2 -co BIGTIFF=YES -init nan --config GDAL_CACHEMAX 2000 -of GTiff -o "{}" {}').format(
        file1_mosaic, file1_string)
    print(gdal_expression)
    subprocess.check_output(gdal_expression, shell=True)


# input_mask = input0['data_path']
# output_x = '{}.tif'.format(os.path.splitext(input_mask)[0])
# gdal_expression = (
#     'gdal_translate -of GTiff -co COMPRESS=DEFLATE -co ZLEVEL=9 -co PREDICTOR=2 -co BIGTIFF=YES "{}" "{}"').format(
#     input_mask, output_x)
# print(gdal_expression)
# subprocess.check_output(gdal_expression, shell=True)

# for i, file_name in enumerate(input_files['file_x1']):
#     print(i, file_name)
#     output_x = '{}.tif'.format(os.path.splitext(file_name)[0])
#     gdal_expression = (
#         'gdal_translate -of GTiff -co COMPRESS=DEFLATE -co ZLEVEL=9 -co PREDICTOR=2 -co BIGTIFF=YES "{}" "{}"').format(
#         file_name, output_x)
#     print(gdal_expression)
#     subprocess.check_output(gdal_expression, shell=True)
