import os
import sys
import subprocess


def set_input_data():
    """

    :rtype: dictionary of input files
    """
    os.chdir(sys.path[0])
    print(sys.path[0])
    data_path = '/Users/xuliang/Documents/yy/hensen_loss'
    data_files = {'data_path': data_path,
                  'out_path': os.path.join(data_path, 'out'),
                  'file_in': os.path.join(data_path, 'loss_year.tif'),
                  'file_out': [os.path.join(data_path, r) for r in
                               "loss_year2001.tif loss_year2002.tif loss_year2003.tif loss_year2004.tif "
                               "loss_year2005.tif loss_year2006.tif loss_year2007.tif loss_year2008.tif "
                               "loss_year2009.tif loss_year2010.tif loss_year2011.tif loss_year2012.tif "
                               "loss_year2013.tif loss_year2014.tif"
                                   .split()],
                  }
    return data_files


ds0 = set_input_data()
input0 = ds0['file_in']
for i, file_name in enumerate(ds0['file_out']):
    print(i, file_name)
    loss_year = i + 1
    print(loss_year)
    expression0 = 'A=={}'.format(loss_year)
    gdal_cmmd = (
        'gdal_calc.py --creation-option COMPRESS=DEFLATE --creation-option ZLEVEL=9 --creation-option PREDICTOR=2 '
        ' --creation-option BIGTIFF=YES --overwrite --NoDataValue=0 --type=Byte -A "{}" --outfile="{}" --calc="{}"'
        ).format(input0, file_name, expression0)
    print(gdal_cmmd)
    subprocess.check_output(gdal_cmmd, shell=True)
