import os
import sys
import subprocess

import raster_tools as rt
# import data_learning as dl


def set_input_data():
    """

    :rtype: dictionary of input files
    """
    os.chdir(sys.path[0])
    print(sys.path[0])
    data_path = os.path.join('..', 'data', 'p06_drc_lidar', '002_DRC_country')
    data_files = {'data_path': data_path,
                  'out_path': os.path.join(data_path, 'output'),
                  'file_mask': os.path.join(data_path, 'input', '008_LC_map_final.tif'),
                  'file_y': os.path.join(data_path, 'input', '014_lidarAgb_201602a_2.tif'),
                  'file_x': [os.path.join(data_path, 'input', r) for r in
                             "014_lidarAgb_201602a_2.tif 008_hhmean_17.tif 008_hhmean_18.tif 008_hvmean_17.tif 008_hvmean_18.tif "
                             "008_srtm_asterfilled_10.tif 008_srtm_asterfilled_11.tif "
                             "008_tm00_10.tif 008_tm00_11.tif 008_tm00_21.tif 008_tm00_22.tif "
                             "008_tm00_32.tif 008_tm00_33.tif 008_tm00_43.tif 008_tm00_44.tif "
                             "008_tm14_10.tif 008_tm14_11.tif 008_tm14_21.tif 008_tm14_22.tif "
                             "008_tm14_32.tif 008_tm14_33.tif 008_tm14_43.tif 008_tm14_44.tif"
                                 .split()]
                  }
    return data_files


def set_input_data_2():
    """

    :rtype: dictionary of input files
    """
    os.chdir(sys.path[0])
    print(sys.path[0])
    data_path = os.path.join('..', 'data', 'tmp')
    data_files = {'data_path': data_path,
                  'out_path': os.path.join(data_path, 'output'),
                  'file_mask': os.path.join(data_path, 'mask.tif'),
                  'file_y': os.path.join(data_path, 'test00.tif'),
                  'file_x': [os.path.join(data_path, r) for r in
                             "test00.tif test01.tif test02.tif test03.tif test04.tif test05.tif"
                                 .split()]
                  }
    return data_files


def set_input_data_3():
    """
    Windows version of test input files
    :rtype: dictionary of input files
    """
    os.chdir(sys.path[0])
    print(sys.path[0])
    data_path = os.path.join('D:\\', 'tmp')
    data_files = {'data_path': data_path,
                  'out_path': os.path.join(data_path, 'output'),
                  'file_mask': os.path.join(data_path, 'mask.tif'),
                  'file_y': os.path.join(data_path, 'test01.tif'),
                  'file_x': [os.path.join(data_path, r) for r in
                             "test01.tif test02.tif test03.tif"
                                 .split()]
                  }
    return data_files


def set_input_data_4():
    """

    :rtype: dictionary of input files
    """
    os.chdir(sys.path[0])
    print(sys.path[0])
    data_path = os.path.join('..', 'data', 'p06_drc_lidar', '002_DRC_country')
    data_files = {'data_path': data_path,
                  'out_path': os.path.join(data_path, 'output'),
                  'file_mask': os.path.join(data_path, 'input', '008_LC_map_final.tif'),
                  'file_y': os.path.join(data_path, 'input', '014_lidarAgb_201602a_2.tif'),
                  'file_x': [os.path.join(data_path, 'input', r) for r in
                             "drc_gain.tif drc_loss.tif"
                                 .split()]
                  }
    return data_files


def set_input_data_5():
    """

    :rtype: dictionary of input files
    """
    os.chdir(sys.path[0])
    print(sys.path[0])
    data_path = os.path.join('..', 'data', 'p06_drc_lidar', '002_DRC_country')
    data_files = {'data_path': data_path,
                  'out_path': os.path.join(data_path, 'input'),
                  'file_mask': os.path.join(data_path, 'input', '008_LC_map_final.tif'),
                  # 'file_mask': os.path.join('..', 'data', 'tmp', 'gee_roc', 'lc8toa_roc.tif'),
                  # 'file_y': os.path.join(data_path, 'googleee', 'b4567nbarmedian.tif'),
                  # 'file_x': [os.path.join(data_path, 'googleee', 'lc8toa_nbar_drc_w45', r) for r in
                  #            "lc8toa_nbar_drc_w45_b1.tif lc8toa_nbar_drc_w45_b2.tif lc8toa_nbar_drc_w45_b3.tif "
                  #            "lc8toa_nbar_drc_w45_b4.tif"
                  #                .split()]
                  'file_x': [os.path.join(data_path, 'googleee', 'le7toa_nbar_drc_2000-03', r) for r in
                             "le7_nbar_drc_2000_b1.tif le7_nbar_drc_2000_b2.tif le7_nbar_drc_2000_b3.tif "
                             "le7_nbar_drc_2000_b4.tif"
                                 .split()]
                  # 'file_x': [os.path.join('..', 'data', 'tmp', 'gee_roc', r) for r in
                  #            "lc8toa_roc.tif"
                  #                .split()]
                  }
    return data_files

input_files = set_input_data_5()
input_mask = input_files['file_mask']

for i, file_name in enumerate(input_files['file_x']):
    print(i, file_name)
    output_x = '{}_100m.tif'.format(os.path.splitext(file_name)[0])
    rt.raster_clip(input_mask, file_name, output_x, 'near')
    output_xf = '{}_filled.tif'.format(os.path.splitext(output_x)[0])
    gdal_expression = (
        'gdal_fillnodata.py -md 5 "{}" -of GTiff -co COMPRESS=LZW -co PREDICTOR=2 -co BIGTIFF=YES "{}"').format(
        output_x, output_xf)
    print(gdal_expression)
    subprocess.check_output(gdal_expression, shell=True)

# file_name0 = input_files['file_y']
# for i in range(4):
#     output_xi = '{}_b{}.tif'.format(os.path.splitext(file_name0)[0], i+1)
#     # gdal_expression = (
#     #     'gdal_translate -ot Float32 -a_nodata nan -b {} -of GTiff -co COMPRESS=LZW -co PREDICTOR=2 -co BIGTIFF=YES "{}" "{}"').format(
#     #     i+1, file_name0, output_xi)
#     # print(gdal_expression)
#     # subprocess.check_output(gdal_expression, shell=True)
#
#     output_xf = '{}_filled.tif'.format(os.path.splitext(output_xi)[0])
#     # gdal_expression = (
#     #     'gdal_fillnodata.py -md 5 "{}" -of GTiff -co COMPRESS=LZW -co PREDICTOR=2 -co BIGTIFF=YES "{}"').format(
#     #     output_xi, output_xf)
#     # print(gdal_expression)
#     # subprocess.check_output(gdal_expression, shell=True)
#
#     output_x = '{}_bicubic_roc_30m.tif'.format(os.path.splitext(output_xf)[0])
#     # rt.raster_clip(input_mask, output_xf, output_x, resampling_method='cubic', srcnodata=0)
#
#     output_xi = '{}_b{}.tif'.format(os.path.splitext(input_mask)[0], i+1)
#     # gdal_expression = (
#     #     'gdal_translate -b {} -of GTiff -co COMPRESS=LZW -co PREDICTOR=2 -co BIGTIFF=YES "{}" "{}"').format(
#     #     i + 1, input_mask, output_xi)
#     # print(gdal_expression)
#     # subprocess.check_output(gdal_expression, shell=True)
#
#     output_xf = '{}_filled.tif'.format(os.path.splitext(output_xi)[0])
#     gdal_expression = (
#         'gdal_fillnodata.py -md 5 "{}" -of GTiff -co COMPRESS=LZW -co PREDICTOR=2 -co BIGTIFF=YES "{}"').format(
#         output_xi, output_xf)
#     print(gdal_expression)
#     subprocess.check_output(gdal_expression, shell=True)



# in_file_list = os.path.join(input_files['out_path'], 'input_file_test.txt')
# with open(in_file_list, "w") as txt0:
#     print(input_mask, file=txt0)
#     for i, file_name in enumerate(input_files['file_x']):
#         print(i, file_name)
#         output_x = '{}_100m.tif'.format(os.path.splitext(file_name)[0])
#         output_x = os.path.join(input_files['out_path'], 'input{:02d}.tif'.format(i))
#         rt.raster_clip(input_mask, file_name, output_x, 'average')
#         print(output_x, file=txt0)

# out_file = os.path.join(input_files['out_path'], 'input_set')
# rt.raster_to_csv(in_file_list, out_file, 0, 0)

# y_file = '{}.csv'.format(out_file)
# svm_file = '{}.libsvm'.format(out_file)
# rt.csv_to_libsvm(y_file, 1, svm_file, mask_column=0)

# subset_file = '{}_subset.csv'.format(out_file)
# # rt.csv_to_subsample(y_file, subset_file, 0.01)
# learn_xgb = dl.GridLearn(subset_file, 1, 0)
# learn_xgb.setup_xgb_model(0.1)
# params = {
#     'pca__n_components': [2, 3, 4],
# }
# learn_xgb.tune_param_set(params)
# params = {
#     'learn__n_estimators': list(range(50, 501, 50)),
# }
# learn_xgb.tune_param_set(params)
# params = {
#     'learn__max_depth': list(range(3, 10, 2)),
#     'learn__min_child_weight': list(range(1, 11, 3)),
# }
# learn_xgb.tune_param_set(params)
# # params = {
# #     'learn__gamma': [i/10.0 for i in range(0,5)],
# # }
# # learn_xgb.tune_param_set(params)
# params = {
#     'learn__n_estimators': list(range(50, 501, 50)),
# }
# learn_xgb.tune_param_set(params)
# params = {
#     'learn__subsample': [i/10.0 for i in range(1,10,2)],
#     'learn__colsample_bytree': [i/10.0 for i in range(5,10,2)],
# }
# learn_xgb.tune_param_set(params)
# params = {
#     'learn__reg_alpha': [0, 1e-4, 1e-2, 1, 100],
# }
# learn_xgb.tune_param_set(params)
# params = {
#     'learn__n_estimators': list(range(50, 501, 50)),
# }
# learn_xgb.tune_param_set(params)
#
# subset1 = '{}_subset1.csv'.format(out_file)
# # rt.csv_to_subsample(y_file, subset1, 0.01)
# subset1libsvm = '{}.libsvm'.format(os.path.splitext(subset1)[0])
# # rt.csv_to_libsvm(subset1, 1, subset1libsvm, mask_column=0)
# subset2 = '{}_subset2.csv'.format(out_file)
# # rt.csv_to_subsample(y_file, subset2, 0.01)
# subset2libsvm = '{}.libsvm'.format(os.path.splitext(subset2)[0])
# # rt.csv_to_libsvm(subset2, 1, subset2libsvm, mask_column=0)
#
# learn_xgb.sklearn_test(subset2, plot_limit=(0, 50))
#
# # dl.learning_xgb_big(subset1libsvm, learn_xgb.mdl.named_steps['learn'], subset2libsvm, plot_limit=(0, 45))

# z_file = '{}.csv'.format(out_file)
# location_file = '{}_location.csv'.format(out_file)
# xyz_file = '{}_xyz.csv'.format(out_file)
# rt.csv_to_ogrvrt(location_file, 0, 1, z_file, 1, xyz_file)
#
# raster_file = '{}.tif'.format(out_file)
# rt.ogrvrt_to_grid(input_mask, xyz_file, raster_file)

