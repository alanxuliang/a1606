import os
import sys
import pandas as pd
import subprocess

import raster_tools as rt


# import data_learning as dl


def set_input_data():
    """

    :rtype: dictionary of input files
    """
    os.chdir(sys.path[0])
    print(sys.path[0])
    data_path = '/Volumes/MyBookThunderboltDuo/Archived_projects/for_yifan/biomass_sa'
    data_files = {'data_path': data_path,
                  'out_path': os.path.join(data_path, 'output'),
                  'file_mask': os.path.join(data_path, 'input', 'alos_2007_global_3sec_mask_cut_sam.byt'),
                  'file_x1': [os.path.join(data_path, 'input', r) for r in
                              "alos_2007_global_3sec_mask_cut_sam.byt alos_2007_global_3sec_hh_cut_landsatfill_sam.int "
                              "alos_2007_global_3sec_hv_cut_landsatfill_sam.int "
                              "global_srtm3_aster_dem_modgrid100m_sam.int global_srtm3_aster_stdev_modgrid100m_sam.int "
                              "tm2015_nir_modgrid100m_sam.byt tm2015_swirb5_modgrid100m_sam.byt "
                              "tm2015_swirb7_modgrid100m_sam.byt"
                                  .split()],
                  }
    return data_files

def set_input_data2():
    """

    :rtype: dictionary of input files
    """
    os.chdir(sys.path[0])
    print(sys.path[0])
    data_path = '/Volumes/MyBookThunderboltDuo/Archived_projects/for_yifan/biomass_sa'
    data_files = {'data_path': data_path,
                  'out_path': os.path.join(data_path, 'output'),
                  'file_mask': os.path.join(data_path, 'input', '250m', 'training_mask.tif'),
                  # 'file_mask': [os.path.join(data_path, 'input/250m/binary', r) for r in
                  #               "mod44b_vcf_2005_global_mosaic_250m_sam.byt sam_srtm_std_v2.bil sam_srtm.bil".split()],
                  'file_x1': [os.path.join(data_path, 'input', r) for r in
                              # "100m/mask.tif"
                              "100m/alos07hh.tif 100m/alos07hv.tif 100m/tm15b4.tif 100m/tm15b5.tif 100m/tm15b7.tif "
                              "250m/modvcf05.tif 250m/srtm3avg.tif 250m/srtm3std.tif"
                                  .split()],
                  }
    return data_files

in0 = set_input_data2()
input_mask = in0['file_mask']
# for i, file_name in enumerate(input_mask):
#     print(i, file_name)
#     output_x = '{}.tif'.format(os.path.splitext(file_name)[0])
#     gdal_expression = (
#         'gdal_translate -of GTiff -co COMPRESS=DEFLATE -co ZLEVEL=9 -co PREDICTOR=2 -co BIGTIFF=YES "{}" "{}"').format(
#         file_name, output_x)
#     print(gdal_expression)
#     subprocess.check_output(gdal_expression, shell=True)


## rasterize csv to geotiff using mask as reference
ref_file = os.path.join(in0['data_path'], 'input', '250m', 'modvcf05_reg.tif')
out_csv = os.path.join(in0['data_path'], 'input', '250m', 'training.csv')
out_y = '{}_mask'.format(os.path.splitext(out_csv)[0])
rt.ogrvrt_to_grid(ref_file, out_csv, 'center_x', 'center_y', 'agb', out_y, a_interp='nearest')
# rt.ogrvrt_to_grid(ref_file, out_csv, 'modsin_x', 'modsin_y', 'agb', out_y, a_interp='average:min_points=1')

## register input files to mask
in_file_list = os.path.join(in0['out_path'], 'input_file_test.txt')
# rt.raster_clip_batch1(input_mask, in_file_list, in0['file_x1'])

## make vrt file for all input files
out_vrt = os.path.join(in0['out_path'], 'biomass_sa.vrt')
field_names = rt.build_stack_vrt(in_file_list, out_vrt)

## save valid pixels to hdf5
out_h5 = '{}.h5'.format(os.path.splitext(out_vrt)[0])
rt.raster_to_h5(out_vrt, out_h5, field_names, 'training_mask_b1', 0, 100)

# ## convert hdf5 to csv
# with pd.HDFStore(out_h5, mode='r') as store:
#     df_columns = store.get_storer('df0').data_columns
#     print(df_columns)
# out_csv = '{}.csv'.format(os.path.splitext(out_h5)[0])
# rt.h5_to_csv(out_h5, out_csv)
